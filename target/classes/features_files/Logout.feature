@Logout

Feature: Test de la deconnexion
  En tant qu'utilisateur je souhaite tester la deconnexion de l'application Swag Labs

  @deconnexion
  Scenario: Je souhaite tester la deconnexion
    Given je me connecte a l'application Swag Labs
    When je clique sur le bouton burger
    And je clique sur le lien Logout
    Then je verifie que le bouton login s'affiche