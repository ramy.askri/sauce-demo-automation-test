@LoginOutline
Feature: Je souhaite me connecter à l'application de Swag Lab avec plusieurs utilisateurs

  Scenario Outline: Je souhaite me connecter à l'application de Swag Lab avec plusieurs utilisateurs
    Given j'ouvre le site de Swag Labs
    When je saisi le username "<username>"
    And je saisi le password "<password>"
    And je clique sur le bouton login
    Then je verifie que le mot "Products" s'affiche
    
  Examples: 
      | username                | password     |
      | standard_user           | secret_sauce |
      | problem_user            | secret_sauce |
      | performance_glitch_user | secret_sauce |
      | error_user              | secret_sauce |
      | visual_user             | secret_sauce |