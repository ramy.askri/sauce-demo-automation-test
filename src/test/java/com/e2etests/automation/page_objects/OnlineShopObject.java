package com.e2etests.automation.page_objects;

import java.time.Duration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.e2etests.automation.utils.Setup;

public class OnlineShopObject {
	
	//Constructeur
	public OnlineShopObject(){
		PageFactory.initElements(Setup.getDriver(), this);
	}
	
	//Déclaration des WebElement
	@FindBy(how=How.ID, using="item_4_title_link")
	public static WebElement productTitle;
	
	@FindBy(how = How.ID, using = "back-to-products")
	public static WebElement returnBtn;
	
	@FindBy(how = How.ID, using = "add-to-cart-sauce-labs-backpack")
	public static WebElement addToCartBtn;
	
	@FindBy(how = How.CLASS_NAME, using = "shopping_cart_badge")
	public static WebElement cartBadge;
	
	@FindBy(how = How.CLASS_NAME, using = "shopping_cart_link")
	public static WebElement cartLink;
	
	@FindBy(how = How.ID, using = "cart_contents_container")
	public static WebElement cartContent;
	
	@FindBy(how = How.ID, using = "checkout")
	public static WebElement checkoutBtn;
	
	@FindBy(how = How.ID, using = "first-name")
	public static WebElement firstName;
	
	@FindBy(how = How.ID, using = "last-name")
	public static WebElement lastName;
	
	@FindBy(how = How.ID, using = "postal-code")
	public static WebElement postalCode;
	
	@FindBy(how = How.ID, using = "continue")
	public static WebElement continueBtn;
	
	@FindBy(how = How.CLASS_NAME, using = "summary_total_label")
	public static WebElement totalPrice;
	
	@FindBy(how = How.ID, using = "finish")
	public static WebElement finishBtn;
	
	@FindBy(how = How.CSS, using = "#checkout_complete_container h2")
	public static WebElement confirmationMsg;
	
	
	//Méthodes	
	public void productClick(){
		WebDriverWait wait=new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(productTitle));
		productTitle.click();
	}
	
	public void addToCartBtnClick(){
		addToCartBtn.click();
	}
	
	public void cartClick(){
		cartLink.click();
	}
	
	public void checkoutBtnClick(){
		checkoutBtn.click();
	}
	
	public void fillFirstName(String first_name) {
		firstName.clear();
		firstName.sendKeys(first_name);
	}
	
	public void fillLastName(String last_name) {
		lastName.clear();
		lastName.sendKeys(last_name);
	}
	
	public void fillPostalCode(String postal_code) {
		postalCode.clear();
		postalCode.sendKeys(postal_code);
	}
	
	public void continueBtnClick(){
		continueBtn.click();
	}
	
	public void finishBtnClick(){
		finishBtn.click();
	}
	
}
