package com.e2etests.automation.page_objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.e2etests.automation.utils.Setup;

public class ProductSortByNameObject {
	
	// Constructeur
	public ProductSortByNameObject() {
		PageFactory.initElements(Setup.getDriver(), this);
	}
	
	// Déclaration des WebElemnts et des variables
	@FindBy(how=How.CLASS_NAME, using="product_sort_container")
	public static WebElement product_sort_container;
	
	@FindBy(how=How.CSS, using="option[value='az']")
	public static WebElement option_AtoZ;
	
	@FindBy(how=How.CSS, using="option[value='za']")
	public static WebElement option_ZtoA;
	
	@FindBy(how=How.CLASS_NAME, using="inventory_item_name")
	public List<WebElement> productsNames;
	
	// Variable de classe pour stocker la liste des prix avant l'action de trie
    private List<String> originalNamesList;
    
    
 // Méthodes
 	public void productSortContainerClick() {
 		product_sort_container.click();
 	}
 	
 	public void optionAtoZClick() {
 		option_AtoZ.click();
 	}
 	
 	public List<String> extractNames() {
        // Initialiser une liste pour stocker les noms des produits
        List<String> namesList = new ArrayList<>();

        // Parcourer chaque élément inventory_item_name
        for (WebElement nameElement : productsNames) {
            // Obtener le texte de l'élément inventory_item_name
            String nameText = nameElement.getText();

            // Ajouter le nom à la liste
            namesList.add(nameText);
        }
        
        // Sauvegarder une copie de la liste initiale des noms
        originalNamesList = new ArrayList<>(namesList);

        return namesList;
    }
 	
 	public boolean SortByNameAtoZ() {
		// Extraire les nom en utilisant la méthode extractNames
        List<String> namesList = extractNames();

        // Trier la liste des noms de A à Z
        Collections.sort(namesList);

        // Comparer la liste initiale avec la liste triée
        boolean identicalList = originalNamesList.equals(namesList);

        // Retourner le résultat de comparaison
        return identicalList;
    }
 	
 	public void optionZtoAClick() {
 		option_ZtoA.click();
 	}
 	
 	public boolean SortByNameZtoA() {
		// Extraire les nom en utilisant la méthode extractNames
        List<String> namesList = extractNames();

        // Trier la liste des noms de A à Z
        Collections.sort(namesList, Collections.reverseOrder());

        // Comparer la liste initiale avec la liste triée
        boolean identicalList = originalNamesList.equals(namesList);

        // Retourner le résultat de comparaison
        return identicalList;
    }
}