package com.e2etests.automation.page_objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.e2etests.automation.utils.Setup;

public class ProductSortByPriceObjects {
	
	// Constructeur
	public ProductSortByPriceObjects(){
		PageFactory.initElements(Setup.getDriver(), this);
	}
	
	// Déclaration des WebElement et des variables
	@FindBy(how=How.CLASS_NAME, using="product_sort_container")
	public static WebElement product_sort_container;
	
	@FindBy(how=How.CSS, using="option[value='hilo']")
	public static WebElement option_high_to_low;
	
	@FindBy(how=How.CSS, using="option[value='lohi']")
	public static WebElement option_low_to_high;
	
	@FindBy(how=How.CLASS_NAME, using="inventory_item_price")
	public List<WebElement> productsPrices;
	
	// Variable de classe pour stocker la liste des prix avant l'action de trie
    private List<Float> originalPricesList;
	
	
	// Méthodes
	public void productSortContainerClick() {
		product_sort_container.click();
	}
	
	public void optionHighToLowClick() {
		option_high_to_low.click();
	}
	

    public List<Float> extractPrices() {
        // Initialiser une liste pour stocker les prix sous forme de nombres
        List<Float> pricesList = new ArrayList<>();

        // Parcourer chaque élément inventory_item_price
        for (WebElement priceElement : productsPrices) {
            // Obtener le texte de l'élément inventory_item_price
            String priceText = priceElement.getText();

            // Supprimer le symbole de dollar et convertir en float
            float priceValue = Float.parseFloat(priceText.replace("$", ""));

            // Ajouter le prix à la liste
            pricesList.add(priceValue);
        }
        
        // Sauvegarder une copie de la liste initiale des prix
        originalPricesList = new ArrayList<>(pricesList);

        return pricesList;
    }

	public boolean SortPricesHightToLow() {
		// Extraire les prix en utilisant la méthode extractPrices
        List<Float> pricesList = extractPrices();

        // Trier la liste des prix par ordre décroissant
        Collections.sort(pricesList, Collections.reverseOrder());

        // Comparer la liste initiale avec la liste triée
        boolean identicalList = originalPricesList.equals(pricesList);

        // Retourner le résultat de comparaison
        return identicalList;
    }
	
	
	public void optionLowToHighClick() {
		option_low_to_high.click();
	}
	
	public boolean SortPricesLowToHight() {
		// Extraire les prix en utilisant la méthode extractPrices
        List<Float> pricesList = extractPrices();

        // Trier la liste des prix par ordre croissant
        Collections.sort(pricesList);

        // Comparer la liste initiale avec la liste triée
        boolean identicalList = originalPricesList.equals(pricesList);

        // Retourner le résultat de comparaison
        return identicalList;
    }
	
}
