package com.e2etests.automation.page_objects;

import java.time.Duration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.e2etests.automation.utils.Setup;

public class LogoutObjects {
	
	// Constructeur
	public LogoutObjects(){
		PageFactory.initElements(Setup.getDriver(), this);
	}
	
	//Déclaration des WebElement
	@FindBy(how=How.ID, using="react-burger-menu-btn")
	public static WebElement burgerButton;
	
	@FindBy(how=How.ID, using="logout_sidebar_link")
	public static WebElement linkLogout;
	
	@FindBy (how=How.ID, using="login-button")
	public static WebElement loginBtn;
	
	
	//Methods
	public void ClickburgerButton() {
		//WebDriverWait wait=new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(20));
		//wait.until(ExpectedConditions.elementToBeClickable(burgerButton));
		burgerButton.click();
	}
	
	public void ClickLinkLogout() {
		WebDriverWait wait=new WebDriverWait(Setup.getDriver(), Duration.ofSeconds(20));
		wait.until(ExpectedConditions.elementToBeClickable(linkLogout));
		linkLogout.click();
	}
}
