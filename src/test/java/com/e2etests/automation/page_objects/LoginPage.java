package com.e2etests.automation.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.e2etests.automation.utils.ConfigFileReader;
import com.e2etests.automation.utils.Setup;

public class LoginPage {

	public ConfigFileReader configFileReader; // ConfigFileReader : Classe pour lire le fichier config (fichier qui
	// contien les jeux de données)

	// Déclaration des WebElement
	@FindBy(how = How.ID, using = "user-name")
	public static WebElement username;

	@FindBy(how = How.ID, using = "password")
	public static WebElement password;

	@FindBy(how = How.ID, using = "login-button")
	public static WebElement loginBtn;

	// @FindBy(how=How.XPATH, using="//span[contains(text(),'Products')]")
	@FindBy(how = How.CLASS_NAME, using = "title")
	public static WebElement titlePage;

	@FindBy(how = How.CSS, using = "#login_button_container h3")
	public static WebElement msgErreur;

	// Constructeur
	public LoginPage() {
		configFileReader = new ConfigFileReader();

		PageFactory.initElements(Setup.getDriver(), this);
	}

	// *************** Methods ***********//

	// Pour le cas passant
	public void connectToApp() {
		Setup.getDriver().get(configFileReader.getProperties("authentification.url"));

		fillUsername(configFileReader.getProperties("username"));
		/*
		 * username.clear();
		 * username.sendKeys(configFileReader.getProperties("username"));
		 */

		fillPassowrd(configFileReader.getProperties("password"));
		/*
		 * password.clear();
		 * password.sendKeys(configFileReader.getProperties("password"));
		 */

		clicktnLog();
		// loginBtn.click();
	}

	// Pour le cas non passant
	public void fillUsername(String user) {
		username.clear();
		username.sendKeys(user);
	}

	public void fillPassowrd(String pwd) {
		password.clear();
		password.sendKeys(pwd);
	}

	public void clicktnLog() {
		loginBtn.click();
	}

}