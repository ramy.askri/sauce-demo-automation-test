package com.e2etests.automation.step_definitions;

import org.junit.Assert;

import com.e2etests.automation.page_objects.LoginPage;
import com.e2etests.automation.utils.Setup;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginStepDefinition {
	
	private LoginPage loginPage;
	
	public LoginStepDefinition() {
		loginPage = new LoginPage();
	}
	
	//Pour @connexion-CasPassant
	@Given("je me connecte a l'application Swag Labs")
	public void jeMeConnecteALapplicationSwagLabs() {
	  loginPage.connectToApp();
	}

	@Then("je verifie que le mot {string} s'affiche")
	public void jeVerifieQueLeMotAffiche(String attendu) {
		String actual = LoginPage.titlePage.getText();
         Assert.assertEquals(attendu, actual);
	}
	
	
	//Pour @connexion-CasNonPassant 
	@Given("j'ouvre le site de Swag Labs")
	public void jeMeConnecteSurURLdeSwagLabs() {
		Setup.getDriver().get(loginPage.configFileReader.getProperties("authentification.url"));
	}

	@When("je saisi le username {string}")
	public void jeSaisiLeUsername(String username) {
		loginPage.fillUsername(username);
	}

	@When("je saisi le password {string}")
	public void jeSaisiLePassword(String passowrd) {
		loginPage.fillPassowrd(passowrd);
	}

	@When("je clique sur le bouton login")
	public void jeCliqueSurLeBoutonLogin() {
		loginPage.clicktnLog();
	}
	
	@Then("le message derreur saffiche {string}")
	public void leMessageDerreurSaffiche(String msgErreur) {
	    String msg=LoginPage.msgErreur.getText();
	    Assert.assertEquals(msg,msgErreur);
	}
}