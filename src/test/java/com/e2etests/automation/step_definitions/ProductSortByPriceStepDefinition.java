package com.e2etests.automation.step_definitions;

import org.junit.Assert;

import com.e2etests.automation.page_objects.ProductSortByPriceObjects;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ProductSortByPriceStepDefinition {
	
	private ProductSortByPriceObjects ProductSortByPriceObjects;
	
	// Constructeur
	public ProductSortByPriceStepDefinition() {
		ProductSortByPriceObjects = new ProductSortByPriceObjects();
	}

	@When("je clique sur le champ select en haut à droite")
	public void jeCliqueSurLeChampSelectEnHautÀDroite() {
		ProductSortByPriceObjects.productSortContainerClick();
	}
	
	@And("je clique sur l'option : Price \\(high to low)")
	public void jeCliqueSurLOptionPriceHighToLow() {
		ProductSortByPriceObjects.optionHighToLowClick();
	}
	
	@Then("l'ordre des produits change selon le prix du plus cher au moins cher")
	public void lOrdreDesProduitsChangeSelonLePrixDuPlusChèreAuMoinsChère() {
		Assert.assertTrue(ProductSortByPriceObjects.SortPricesHightToLow());
	}

	@And("je clique sur l'option : Price \\(low to high)")
	public void jeCliqueSurLOptionPriceLowToHigh() {
		ProductSortByPriceObjects.optionLowToHighClick();
	}
	@Then("l'ordre des produits change selon le prix du moins cher au plus cher")
	public void lOrdreDesProduitsChangeSelonLePrixDuMoinsChèreAuPlusChère() {
		Assert.assertTrue(ProductSortByPriceObjects.SortPricesLowToHight());
	}
	
}
