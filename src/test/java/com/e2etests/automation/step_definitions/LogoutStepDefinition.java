package com.e2etests.automation.step_definitions;

import org.junit.Assert;

import com.e2etests.automation.page_objects.LogoutObjects;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class LogoutStepDefinition {

	// Déclaration de variable "logout" de type "Logout"
	private LogoutObjects logout;
	
	public LogoutStepDefinition() {
		// initialisation de la variable "logout" en créant une instance de la classe
		// "Logout" afin de pouvoir utiliser les méthodes de la classe "Logout" par la
		// variable "logout"
		logout = new LogoutObjects();
	}

	@When("je clique sur le bouton burger")
	public void jeCliqueSurLeBoutonBurger() {
		logout.ClickburgerButton();
	}

	@When("je clique sur le lien Logout")
	public void jeCliqueSurLeLienLogout() throws InterruptedException {
		logout.ClickLinkLogout();
	}

	@Then("je verifie que le bouton login s'affiche")
	public void jeVerifieQueLeBoutonLoginAffiche() {
		boolean btnLoginExist = LogoutObjects.loginBtn.isDisplayed();
		Assert.assertEquals(true, btnLoginExist);
	}
}
