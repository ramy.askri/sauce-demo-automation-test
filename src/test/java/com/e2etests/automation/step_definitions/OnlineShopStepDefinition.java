package com.e2etests.automation.step_definitions;

import org.junit.Assert;

import com.e2etests.automation.page_objects.OnlineShopObject;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class OnlineShopStepDefinition {
	
	private OnlineShopObject onlineShopObject;
	
	// Constructeur
	public OnlineShopStepDefinition() {
		onlineShopObject = new OnlineShopObject();
	}

	@When("je clique sur le titre d'un produit")
	public void jeCliqueSurLeTitreDUnProduit() {
		onlineShopObject.productClick();
	}

	@And("la page produit s'ouvre")
	public void laPageProduitSOuvre() {
		Assert.assertTrue(OnlineShopObject.returnBtn.isDisplayed());
	}

	@And("je clique sur le bouton Add to cart")
	public void jeCliqueSurLeBoutonAddToCart() {
		onlineShopObject.addToCartBtnClick();
	}

	@And("le produit s'ajout dans le panier")
	public void leProduitSAjoutDansLePanier() {
		Assert.assertTrue(OnlineShopObject.cartBadge.isDisplayed());
	}

	@And("je clique sur le picto panier en haut à droite")
	public void jeCliqueSurLePictoPanierEnHautÀDroite() {
		onlineShopObject.cartClick();
	}

	@And("le panier s'ouvre")
	public void lePanierSouvre() {
		Assert.assertTrue(OnlineShopObject.cartContent.isDisplayed());
	}

	@And("je clique sur le bouton Checkout")
	public void jeCliqueSurLeBoutonCheckout() {
		onlineShopObject.checkoutBtnClick();
	}

	@And("je saisi le prénom {string} dans le champ First name")
	public void jeSaisiLePrénomDansLeChampFirstName(String first_name) {
		onlineShopObject.fillFirstName(first_name);
	}

	@And("je saisi le nom {string} dans le champ Last name")
	public void jeSaisiLeNomDansLeChampLastName(String last_name) {
		onlineShopObject.fillLastName(last_name);
	}

	@And("je saisi le code postal {string} dans le champ Zip\\/Postal Code")
	public void jeSaisiLeCodePostalDansLeChampZipPostalCode(String postal_code) {
		onlineShopObject.fillPostalCode(postal_code);
	}

	@And("je clique sur le bouton Continue")
	public void jeCliqueSurLeBoutonContinue() {
		onlineShopObject.continueBtnClick();
	}

	@And("le montant total à payer s'affiche")
	public void leMontantTotalÀPayerSAffiche() {
		Assert.assertTrue(OnlineShopObject.totalPrice.isDisplayed());
	}

	@And("je clique sur le bouton Finish")
	public void jeCliqueSurLeBoutonFinish() {
		onlineShopObject.finishBtnClick();
	}

	@Then("la page de confirmation s'affiche avec le message {string}")
	public void laPageDeConfirmationSAfficheAvecLeMessageThankYouForYourOrder(String confirmationMessage) {
		Assert.assertTrue(OnlineShopObject.confirmationMsg.isDisplayed());
		
		String msg = OnlineShopObject.confirmationMsg.getText();
        Assert.assertEquals(confirmationMessage, msg);
	}

}
