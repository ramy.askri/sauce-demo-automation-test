package com.e2etests.automation.step_definitions;

import org.junit.Assert;

import com.e2etests.automation.page_objects.ProductSortByNameObject;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ProductSortByNameStepDefinition {
	
	ProductSortByNameObject productSortByNameObject;
	
	public ProductSortByNameStepDefinition() {
		productSortByNameObject = new ProductSortByNameObject();
	}

	@When("je clique sur l'option : Name \\(A to Z)")
	public void jeCliqueSurLOptionNameAToZ() {
		productSortByNameObject.optionAtoZClick();
	}
	
	@Then("l'ordre des produits change selon l'ordre alphabétique de A à Z")
	public void lOrdreDesProduitsChangeSelonLOrdreAlphabétiqueDeAÀZ() {
		Assert.assertTrue(productSortByNameObject.SortByNameAtoZ());
	}

	@When("je clique sur l'option : Name \\(Z to A)")
	public void jeCliqueSurLOptionNameZToA() {
		productSortByNameObject.optionZtoAClick();
	}
	
	@Then("l'ordre des produits change selon l'ordre alphabétique de Z à A")
	public void lOrdreDesProduitsChangeSelonLOrdreAlphabétiqueDeZÀA() {
		Assert.assertTrue(productSortByNameObject.SortByNameZtoA());
	}

	
}
