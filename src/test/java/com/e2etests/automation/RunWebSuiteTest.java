package com.e2etests.automation;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import static io.cucumber.junit.CucumberOptions.SnippetType.CAMELCASE;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "src/spec/features" }, // Chemin du dossier qui contient les features
		plugin = { "pretty", "html:target/cucumber-report.html" }, // Plugins pour générer le rapport
		tags = (""), // Les tags des features ou scénarios qu'on veut éxecuter
		// glue = {"/src/test/java/com/e2etests/automation/step_definitions"},
		// //Décommenter si la classe RunWebSuiteTest et le package step_definitions ne
		// sont pas au même niveau dans le dossier du projet
		monochrome = true, // rend les sorties dans la console lisible
		snippets = CAMELCASE)

public class RunWebSuiteTest {

}