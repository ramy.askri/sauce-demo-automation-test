@OnlineShop
Feature: Achat en ligne

Background:

    Given je me connecte a l'application Swag Labs
    When je clique sur le titre d'un produit
    And la page produit s'ouvre
    And je clique sur le bouton Add to cart
    And le produit s'ajout dans le panier
    And je clique sur le picto panier en haut à droite
    And le panier s'ouvre
    
    @passer-commande-Ramy
    Scenario: En tant que utilisateur je veux ajouter mes coordonnee
    
    And je clique sur le bouton Checkout
    And je saisi le prénom "Ramy" dans le champ First name
    And je saisi le nom "Askri" dans le champ Last name
    And je saisi le code postal "2040" dans le champ Zip/Postal Code
    And je clique sur le bouton Continue
    And le montant total à payer s'affiche
    And je clique sur le bouton Finish
    Then la page de confirmation s'affiche avec le message "Thank you for your order!"
    
    @passer-commande-Lobna
    Scenario: En tant que utilisateur je veux ajouter mes coordonnee
    
    And je clique sur le bouton Checkout
    And je saisi le prénom "Lobna" dans le champ First name
    And je saisi le nom "Askri" dans le champ Last name
    And je saisi le code postal "2040" dans le champ Zip/Postal Code
    And je clique sur le bouton Continue
    And le montant total à payer s'affiche
    And je clique sur le bouton Finish
    Then la page de confirmation s'affiche avec le message "Thank you for your order!"