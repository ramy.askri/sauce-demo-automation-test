@Login
Feature: test de la page d authentification
  ETQ utilisateur je souhaite tester la page de la connexion de l application Swag Labs

  @connexion-CasPassant
  Scenario: Je souhaite tester la page de la connexion avec un cas passant
    Given je me connecte a l'application Swag Labs
    Then je verifie que le mot "Products" s'affiche

  @connexion-CasNonPassant-1
  Scenario: Je souhaite tester la page de connexion avec un faut Login et un Faut Password
    Given j'ouvre le site de Swag Labs
    When je saisi le username "ramy"
    And je saisi le password "123"
    And je clique sur le bouton login
    Then le message derreur saffiche "Epic sadface: Username and password do not match any user in this service"

  @connexion-CasNonPassant-2
  Scenario: Je souhaite tester la page de connexion avec un bon Login et un faut Password
    Given j'ouvre le site de Swag Labs
    When je saisi le username "standard_user"
    And je saisi le password "123"
    And je clique sur le bouton login
    Then le message derreur saffiche "Epic sadface: Username and password do not match any user in this service"

  @connexion-CasNonPassant-3
  Scenario: Je souhaite tester la page de connexion avec un faut Login et un bon Password
    Given j'ouvre le site de Swag Labs
    When je saisi le username "ramy"
    And je saisi le password "secret_sauce"
    And je clique sur le bouton login
    Then le message derreur saffiche "Epic sadface: Username and password do not match any user in this service"

# Si on veut utiliser la langue française pour l'écriture du code on mets au début du fichier :    
# #language:fr
# puis les commandes devient :
# Feature: => Fonctionnalité:
# Scenario: => Scénario:
# Given => Etant donné
# When => Quand
# And => Alors
# Then => Et   
