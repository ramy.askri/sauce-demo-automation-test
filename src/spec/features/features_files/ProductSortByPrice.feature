@Product-Sort-By-Price
Feature: Trie des produits par prix
  En tant qu'utilisateur je veux afficher les produits selon le prix par ordre décroissant ou croissant

Background:
		Given je me connecte a l'application Swag Labs
    When je clique sur le champ select en haut à droite

  @hight-to-low
  Scenario: Je souhaite afficher les produits par ordre décroissant selon le prix
    And je clique sur l'option : Price (high to low)
    Then l'ordre des produits change selon le prix du plus cher au moins cher
    
  @low-to-hight
  Scenario: Je souhaite afficher les produits par ordre croissant selon le prix
    And je clique sur l'option : Price (low to high)
    Then l'ordre des produits change selon le prix du moins cher au plus cher