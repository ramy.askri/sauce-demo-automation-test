@Product-Sort-By-Name
Feature: Trie des produits par nom
  En tant qu'utilisateur je veux afficher les produits par ordre alphabétique

Background:
		Given je me connecte a l'application Swag Labs
    When je clique sur le champ select en haut à droite

  @A-to-Z
  Scenario: Je souhaite afficher les produits par nom par ordre alphabétique de A à Z
    And je clique sur l'option : Name (A to Z)
    Then l'ordre des produits change selon l'ordre alphabétique de A à Z
    
  @Z-to-A
  Scenario: Je souhaite afficher les produits par nom par ordre alphabétique de Z à A
    And je clique sur l'option : Name (Z to A)
    Then l'ordre des produits change selon l'ordre alphabétique de Z à A